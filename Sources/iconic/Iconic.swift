//
//  Iconic.swift
//  
//  Created by Matthew Flint on 13-May-2020.
//

import Foundation

struct Iconic {
	private let config: IconicArguments

	init?() {
		if let colorHelpArguments = try? ColorHelpArguments.parse(),
			colorHelpArguments.listColors == true {
			print("Available colors:")
			for colorName in IconicColor.allCases {
				print ("  \(colorName)")
			}
			return nil
		}

		self.config = IconicArguments.parseOrExit()
	}

	init(withConfig config: IconicArguments) {
		self.config = config
		}

	func generate() {
		do {
			try self.doGenerate()
		} catch {
			print(error.localizedDescription)
		}
	}

	private func doGenerate() throws {
		let colorScheme: ColorScheme

		if let foregroundColorName = self.config.foregroundColor,
			let backgroundColorName = self.config.backgroundColor {
			colorScheme = try IconicColor.colorScheme(foregroundColorName: foregroundColorName, backgroundColorName: backgroundColorName)
		} else {
			colorScheme = IconicColor.random()
		}

		let outputDirectory: URL
		if let outputDirectoryString = config.outputDirectory {
			outputDirectory = URL(fileURLWithPath: outputDirectoryString)
			guard outputDirectory.hasDirectoryPath else {
				throw IconicError.notDirectory
			}
		} else {
			outputDirectory = URL(fileURLWithPath: FileManager.default.currentDirectoryPath)
		}

		print("Searching \(outputDirectory.absoluteString)")

		let contentDetails = try self.findAppIconContent(in: outputDirectory, searchRootPath: outputDirectory, possibleAppIconSetDirectory: nil)
		print("Found \(contentDetails.count) AppIcon iconsets")

		for contentDetail in contentDetails {
			// get all the `idiom` values from the Contents.json file
			let idiomsInContent = contentDetail.contents.images.map { image in
				image.idiom
			}

			// detect the kind of iconSet (iOS/macOS/etc)
			guard let iconSet = IconSet.iconSet(for: idiomsInContent) else {
				print("⚠️ Could not identify type of icon set in: \(contentDetail.relativeAppIconSetPath)")
				continue
			}

			try self.replace(iconSet: iconSet, contentDetail: contentDetail, colorScheme: colorScheme)
		}
	}

	private func findAppIconContent(in url: URL, searchRootPath: URL, possibleAppIconSetDirectory: URL?) throws -> [ContentDetail] {
		var result = [ContentDetail]()

		// Assets.xcassets/AppIcon.appiconset/Contents.json
		let reversedPathComponents: [String] = url.pathComponents.reversed()
		if reversedPathComponents.count >= 3,
			let possibleAppIconSetDirectory = possibleAppIconSetDirectory,
			reversedPathComponents[0] == "Contents.json",
			reversedPathComponents[1] == "AppIcon.appiconset",
			reversedPathComponents[2] == "Assets.xcassets",
			let jsonData = try String(contentsOf: url, encoding: .utf8).data(using: .utf8) {

			let contents = try JSONDecoder().decode(Contents.self, from: jsonData)
			let contentDetail = ContentDetail(appIconSetDirectory: possibleAppIconSetDirectory, contentsFile: url, contents: contents, searchRootPath: searchRootPath)
			result.append(contentDetail)
		}

		if url.hasDirectoryPath {
			for child in try FileManager.default.contentsOfDirectory(at: url, includingPropertiesForKeys: nil, options: []) {
				result.append(contentsOf: try self.findAppIconContent(in: child, searchRootPath: searchRootPath, possibleAppIconSetDirectory: url))
			}
		}

		return result
	}

	private func replace(iconSet: IconSet, contentDetail: ContentDetail, colorScheme: ColorScheme) throws {
		let filesToDelete = contentDetail.contents.images.compactMap { image in
			image.filename
		}.map { filename in
			URL(fileURLWithPath: filename, relativeTo: contentDetail.appIconSetDirectory)
		}

		let deletionSet = Set(filesToDelete)

		var deleteCount = 0
		for fileToDelete in deletionSet {
			do {
				try FileManager.default.removeItem(at: fileToDelete)
				deleteCount += 1
			} catch {
				// ignore
			}
		}

		print("✅ deleted \(deleteCount) existing \(iconSet.name()) images from \(contentDetail.relativeAppIconSetPath)")

		var contents = Contents(images: [], info: Info(version: 1, author: "xcode"))

		let iconsToGenerate = iconSet.icons().filter({ icon -> Bool in
			icon.generate
		})
		for icon in iconsToGenerate {
			let fontSize = icon.size * CGFloat(icon.scale) * 0.55 // empirical measurement
			let glyph = try Font.sfSymbolFont(size: fontSize).glyph(fullName: config.sfSymbolName)
			let sizeString = String(format: "%g", icon.size)
			let outputFileName = "AppIcon-\(sizeString)@\(icon.scale)x.png"
			let outputURL = URL(fileURLWithPath: outputFileName, relativeTo: contentDetail.appIconSetDirectory)
			try IconGenerator(withConfig: self.config).generate(size: icon.size, scale: CGFloat(icon.scale), glyph: glyph, colorScheme: colorScheme, outputURL: outputURL)

			contents.images.append(Image(idiom: icon.idiom, size: "\(sizeString)x\(sizeString)", scale: "\(icon.scale)x", filename: outputFileName))
		}

		print("✅ created \(iconsToGenerate.count) \(iconSet.name()) images in \(contentDetail.relativeAppIconSetPath)")

		let encoder = JSONEncoder()
		encoder.outputFormatting = [.prettyPrinted, .sortedKeys]
		let contentsData = try encoder.encode(contents)
		try contentsData.write(to: contentDetail.contentsFile)

		print("✅ wrote new Contents.json in \(contentDetail.relativeAppIconSetPath)")
	}
}

struct ContentDetail {
	let relativeAppIconSetPath: String
	let appIconSetDirectory: URL
	let contentsFile: URL
	let contents: Contents

	init(appIconSetDirectory: URL, contentsFile: URL, contents: Contents, searchRootPath: URL) {
		var appIconSetDirectoryString = appIconSetDirectory.absoluteString
		let searchRootPathString = searchRootPath.absoluteString
		guard let range = appIconSetDirectoryString.range(of: searchRootPathString) else {
			preconditionFailure()
		}
		appIconSetDirectoryString.removeSubrange(range)
		self.relativeAppIconSetPath = appIconSetDirectoryString

		self.appIconSetDirectory = appIconSetDirectory
		self.contentsFile = contentsFile
		self.contents = contents
	}
}
