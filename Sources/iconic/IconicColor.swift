//
//  IconicColor.swift
//
//  Created by Matthew Flint on 14-May-2020.
//

import Cocoa

struct ColorScheme {
	let foreground: CGColor
	let background: CGColor
}

enum IconicColor: String, CaseIterable {
	case black
	case darkGray
	case lightGray
	case white
	case gray
	case red
	case green
	case blue
	case cyan
	case yellow
	case magenta
	case orange
	case purple
	case brown

	func cgColor() -> CGColor {
		switch self {
		case .black: return NSColor.black.cgColor
		case .darkGray: return NSColor.darkGray.cgColor
		case .lightGray: return NSColor.lightGray.cgColor
		case .white: return NSColor.white.cgColor
		case .gray: return NSColor.gray.cgColor
		case .red: return NSColor.red.cgColor
		case .green: return NSColor.green.cgColor
		case .blue: return NSColor.blue.cgColor
		case .cyan: return NSColor.cyan.cgColor
		case .yellow: return NSColor.yellow.cgColor
		case .magenta: return NSColor.magenta.cgColor
		case .orange: return NSColor.orange.cgColor
		case .purple: return NSColor.purple.cgColor
		case .brown: return NSColor.brown.cgColor
		}
	}

	static func colorScheme(foregroundColorName: String, backgroundColorName: String) throws -> ColorScheme {
		guard let foregroundColor = IconicColor(rawValue: foregroundColorName)?.cgColor() else {
			throw IconicError.badColor(name: foregroundColorName)
		}

		guard let backgroundColor = IconicColor(rawValue: backgroundColorName)?.cgColor() else {
			throw IconicError.badColor(name: backgroundColorName)
		}

		return ColorScheme(foreground: foregroundColor, background: backgroundColor)
	}

	static func random() -> ColorScheme {
		let lightForeground = Bool.random()

		let foregroundHue = CGFloat.random(in: 0...1)
		let foregroundSaturation: CGFloat = CGFloat.random(in: 0.6...1)
		let foregroundBrightness: CGFloat = lightForeground ? 0.9 : 0.3
		let foregroundColor = NSColor(hue: foregroundHue, saturation: foregroundSaturation, brightness: foregroundBrightness, alpha: CGFloat(1))

		let backgroundHue = (foregroundColor.hueComponent + CGFloat.random(in: 0.4...0.6)).truncatingRemainder(dividingBy: 1)
		let backgroundSaturation: CGFloat = CGFloat.random(in: 0...0.4);
		let backgroundBrightness: CGFloat = lightForeground ? 0.3 : 0.9
		let backgroundColor = NSColor(hue: backgroundHue, saturation: backgroundSaturation, brightness: backgroundBrightness, alpha: CGFloat(1))

		return ColorScheme(foreground: foregroundColor.cgColor, background: backgroundColor.cgColor)
	}
}
