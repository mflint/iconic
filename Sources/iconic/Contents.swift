//
//  Contents.swift
//  
//
//  Created by Matthew Flint on 13-May-2020.
//

import Foundation

struct Contents: Codable {
	var images: [Image]
	var info: Info
}

struct Image: Codable {
	let idiom: Idiom
	let size: String
	let scale: String
	let filename: String?
}

struct Info: Codable {
	let version: Int
	let author: String
}
