//
//  Font.swift
//
//  Created by Matthew Flint on 13-May-2020.
//

import Cocoa
import CommonCrypto

struct Font {
	enum Weight: Int {
		case ultraLight = 1
		case thin = 3
		// case light
		case regular = 5
		case medium = 6
		case semibold = 8
		case bold = 9
		case heavy = 10
		case black = 11
	}

	private enum CSVField: Int {
		case fullName = 10
	}

	static func sfSymbolFont(size: CGFloat = 44) throws -> Font {
		guard let font = NSFontManager.shared.font(withFamily: "SF Pro Display", traits: [], weight: Weight.semibold.rawValue, size: size) else {
			throw IconicError.fontNotFound
		}

		return try Font(font: font as CTFont)
	}

	private let csvLines: [String]
	private let font: CTFont

	private init(font: CTFont) throws {
		guard let data = CTFontCopyDecodedSYMPData(font) else {
			throw IconicError.fontNotParsed(reason: "invalid SYMP data")
		}

        guard let csv = String(data: data, encoding: .utf8) else {
			throw IconicError.fontNotParsed(reason: "couldn't decode csv")
		}
		
        // drop the first and last lines (headers + summary, respectively)
		self.csvLines = csv.components(separatedBy: "\r\n").dropFirst().dropLast()
		self.font = font
	}

	func glyph(fullName: String) throws -> Glyph {
		// quickly grep the complete csv strings
		let quickMatchGlyphs = self.csvLines.filter { csvLine -> Bool in
			csvLine.contains(fullName)
		}

		let slowMatchGlyphs = try quickMatchGlyphs.filter { csvLine -> Bool in
			let fields = CSVFields(csvLine)
			if fields.count != 12 {
				throw IconicError.unexpectedCSVFormat(parts: fields.count)
			}
			return fields[CSVField.fullName.rawValue] == fullName
		}

		if slowMatchGlyphs.count > 1 {
			throw IconicError.multipleMatchingGlyphs(count: slowMatchGlyphs.count)
		}

		guard let glyphData = slowMatchGlyphs.first else {
			throw IconicError.glyphNotFound(fullName: fullName)
		}

		guard let glyph = Glyph(size: Glyph.Size.medium, pieces: CSVFields(glyphData), inFont: font) else {
			throw IconicError.glyphNotParsed
		}

		return glyph
	}
}

// Taken from Dave DeLong's "sfsymbols" repo
// See https://github.com/davedelong/sfsymbols/blob/master/Sources/SFSymbolsCore/Font.swift
private func CTFontCopyDecodedSYMPData(_ font: CTFont) -> Data? {
	func fourCharCode(_ string: String) -> FourCharCode {
		return string.utf16.reduce(0, {$0 << 8 + FourCharCode($1)})
	}

	let tag = fourCharCode("symp")
	guard let data = CTFontCopyTable(font, tag, []) else { return nil }
	guard let base64String = String(data: data as Data, encoding: .utf8) else { return nil }
	guard let encoded = NSData(base64Encoded: base64String) else { return nil }
	guard let decoded = NSMutableData(length: encoded.length) else { return nil }

	var key: Array<UInt8> = [0xB8, 0x85, 0xF6, 0x9E, 0x39, 0x8C, 0xBA, 0x72, 0x40, 0xDB, 0x49, 0x6B, 0xE8, 0xC6, 0x14, 0x88, 0x54, 0x9F, 0x1F, 0x88, 0x5D, 0x47, 0x6B, 0x2E, 0x2C, 0xC1, 0x14, 0xF1, 0x3B, 0x17, 0x21, 0x20]
	var iv: Array<UInt8> = [0xEF, 0xB0, 0xD1, 0x2E, 0xFA, 0xC5, 0x91, 0x14, 0xC3, 0xE5, 0xB9, 0x12, 0x70, 0xF0, 0xC0, 0x46]

	var bytesWritten = 0
	let result = CCCrypt(CCOperation(kCCDecrypt),
						 CCAlgorithm(kCCAlgorithmAES),
						 CCOptions(kCCOptionPKCS7Padding),
						 &key, key.count,
						 &iv,
						 encoded.bytes, encoded.length,
						 decoded.mutableBytes, decoded.length,
						 &bytesWritten)

	guard result == kCCSuccess else { return nil }
	decoded.length = bytesWritten
	return decoded as Data
}

// Taken from Dave DeLong's "sfsymbols" repo
// See https://github.com/davedelong/sfsymbols/blob/master/Sources/SFSymbolsCore/Font.swift
internal func CSVFields(_ line: String) -> Array<String> {
	var fields = Array<String>()

	var insideQuote = false
	var fieldStart = line.startIndex
	var currentIndex = fieldStart
	while currentIndex < line.endIndex {
		let character = line[currentIndex]

		if insideQuote == false {
			if character == "," {
				let subString = line[fieldStart ..< currentIndex]
				fields.append(String(subString).trimmingCharacters(in: .whitespaces))
				fieldStart = line.index(after: currentIndex)
			} else if character == "\"" {
				insideQuote = true
			}
		} else {
			if character == "\"" { insideQuote = false }
		}

		if currentIndex >= line.endIndex { break }
		currentIndex = line.index(after: currentIndex)
	}

	let lastField = line[fieldStart ..< line.endIndex]
	fields.append(String(lastField).trimmingCharacters(in: .whitespaces))

	return fields
}

