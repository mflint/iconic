//
//  Errors.swift
//
//  Created by Matthew Flint on 13-May-2020.
//

import Foundation

enum IconicError: Error {
	case badColor(name: String)
	case notDirectory
	case fontNotFound
	case fontNotParsed(reason: String)
	case unexpectedCSVFormat(parts: Int)
	case multipleMatchingGlyphs(count: Int)
	case glyphNotFound(fullName: String)
	case glyphNotParsed
}

extension IconicError: LocalizedError {
    public var errorDescription: String? {switch self {
			case .badColor(let name): return "Unknown color '\(name)'"
			case .notDirectory: return "Output directory is not a directory"
			case .fontNotFound: return "SF Symbols font not found"
			case .fontNotParsed(let reason): return "Could not parse SF Symbols font. (\(reason))"
			case .unexpectedCSVFormat: return "Unexpected CSV format in SF Symbols"
			case .multipleMatchingGlyphs(let count): return "\(count) glyphs found matching the given name"
			case .glyphNotFound(let fullName): return "SF Symbols glyph '\(fullName)' not found"
			case .glyphNotParsed: return "Could not parse glyph data"
		}
    }
}
