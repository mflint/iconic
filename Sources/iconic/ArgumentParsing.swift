//
//  ArgumentParsing.swift
//  
//  Created by Matthew Flint on 13-May-2020.
//

import ArgumentParser

struct ColorHelpArguments: ParsableArguments {
	@Flag(name: .shortAndLong, help: "List available colors and quit")
	var listColors: Bool
}

struct IconicArguments: ParsableArguments {
	@Option(name: .shortAndLong, help: "The name of the SFSymbol to use")
    var sfSymbolName: String

	@Option(name: .shortAndLong, help: "Foreground color for the SFSymbol\nUse --list-colors to get a list of colors")
    var foregroundColor: String?

	@Option(name: .shortAndLong, help: "Background color for the icon\nUse --list-colors to get a list of colors")
    var backgroundColor: String?

	@Option(name: .shortAndLong, help: "Output directory; if not given, the current working directory will be used")
	var outputDirectory: String?

	@Flag(name: .shortAndLong, help: "List available colors and quit")
	var listColors: Bool
}
