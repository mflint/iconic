//
//  IconSet.swift
//  
//  Created by Matthew Flint on 13-May-2020.
//

import Foundation

enum IconSet {
	case iOS
	case macOS
	// TODO: watchOS, tvOS, carPlay?

	private static let iOSIdioms = Set([Idiom.iPhone, Idiom.iPad, Idiom.iOSMarketing])
	private static let macOSIdioms = Set([Idiom.mac])

	// given a set of discovered idioms in a Contents.json file, this identifies the IconSet
	// for that Contents file
	static func iconSet(for idioms: [Idiom]) -> IconSet? {
		let idiomSet = Set(idioms)
		if Self.iOSIdioms.intersection(idiomSet).count == idiomSet.count {
			return .iOS
		}

		if Self.macOSIdioms.intersection(idiomSet).count == idiomSet.count {
			return .macOS
		}

		return nil
	}

	func name() -> String {
		switch self {
		case .iOS: return "iOS"
		case .macOS: return "macOS"
		}
	}

	func icons() -> [Icon] {
		switch self {
		case .iOS: return Icon.iOSIcons
		case .macOS: return Icon.macIcons
		}
	}
}

enum Idiom: String, Codable {
	case iPhone = "iphone"
	case iPad = "ipad"
	case iOSMarketing = "ios-marketing"
	case mac = "mac"
	case watch = "watch"
	case watchMarketing = "watch-marketing"
}

struct Icon {
	let size: CGFloat // points
	let scale: Int
	let idiom: Idiom
	let generate: Bool

	static let iOSIcons = [
		// iPhone notification, iOS 7 onwards
		Icon(size: 20, scale: 2, idiom: .iPhone, generate: true),
		Icon(size: 20, scale: 3, idiom: .iPhone, generate: true),

		// iPhone settings
		Icon(size: 29, scale: 1, idiom: .iPhone, generate: false),
		Icon(size: 29, scale: 2, idiom: .iPhone, generate: true),
		Icon(size: 29, scale: 3, idiom: .iPhone, generate: true),

		// iPhone spotlight
		Icon(size: 40, scale: 2, idiom: .iPhone, generate: true),
		Icon(size: 40, scale: 3, idiom: .iPhone, generate: true),

		// iPhone app, iOS 5,6
		Icon(size: 57, scale: 1, idiom: .iPhone, generate: false),
		Icon(size: 57, scale: 2, idiom: .iPhone, generate: false),

		// iPhone app, iOS 7 onwards
		Icon(size: 60, scale: 2, idiom: .iPhone, generate: true),
		Icon(size: 60, scale: 3, idiom: .iPhone, generate: true),

		// iPad Notifications, iOS 7 onwards
		Icon(size: 20, scale: 1, idiom: .iPad, generate: false),
		Icon(size: 20, scale: 2, idiom: .iPad, generate: true),

		// iPad Settings
		Icon(size: 29, scale: 1, idiom: .iPad, generate: false),
		Icon(size: 29, scale: 2, idiom: .iPad, generate: true),

		// iPad spotlight, iOS 7 onwards
		Icon(size: 40, scale: 1, idiom: .iPad, generate: false),
		Icon(size: 40, scale: 2, idiom: .iPad, generate: true),

		// iPad spotlight, iOS 5,6
		Icon(size: 50, scale: 1, idiom: .iPad, generate: false),
		Icon(size: 50, scale: 2, idiom: .iPad, generate: true),

		// iPad app, iOS 5,6
		Icon(size: 72, scale: 1, idiom: .iPad, generate: false),
		Icon(size: 72, scale: 2, idiom: .iPad, generate: false),

		// iPad app, iOS 7 onwards
		Icon(size: 76, scale: 1, idiom: .iPad, generate: false),
		Icon(size: 76, scale: 2, idiom: .iPad, generate: true),

		// iPad Pro 12.9" app, iOS 9 onwards
		Icon(size: 83.5, scale: 2, idiom: .iPad, generate: true),

		// marketing
		Icon(size: 1024, scale: 1, idiom: .iOSMarketing, generate: true),
	]

	static let macIcons = [
		Icon(size: 16, scale: 1, idiom: .mac, generate: true),
		Icon(size: 16, scale: 2, idiom: .mac, generate: true),
		Icon(size: 32, scale: 1, idiom: .mac, generate: true),
		Icon(size: 32, scale: 2, idiom: .mac, generate: true),
		Icon(size: 128, scale: 1, idiom: .mac, generate: true),
		Icon(size: 128, scale: 2, idiom: .mac, generate: true),
		Icon(size: 256, scale: 1, idiom: .mac, generate: true),
		Icon(size: 256, scale: 2, idiom: .mac, generate: true),
		Icon(size: 512, scale: 1, idiom: .mac, generate: true),
		Icon(size: 512, scale: 2, idiom: .mac, generate: true),
	]
}
