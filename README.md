# iconic

`iconic` is a tool for creating simple iOS and macOS app icons from SF Symbols. It will:

* search the current working directory, or a given directory, for iOS and macOS image asset catalogs
* replace any `AppIcon` imagesets with generated images

Example:

![example](example.png)

**Warning:** this tool works destructively on the output directory, or the current working directory if `--output-directory` is not specified.

## Not for Production

This is for creating temporary icons only.

Apple's SF Symbols documentation says:

> You may not use SF Symbols — or glyphs that are substantially or confusingly similar — in your app icons

For more information, see [https://developer.apple.com/design/human-interface-guidelines/sf-symbols/overview/](https://developer.apple.com/design/human-interface-guidelines/sf-symbols/overview/).

## Installation

```sh
cd iconic
swift build -c release
ln -s ${PWD}/.build/release/iconic /usr/local/bin/iconic
```

## Usage

### Minimal example

The only mandatory option is `--sf-symbol-name`, to specify the symbol name. Random colors will be chosen, and the current working directory will be searched for image asset catalogs:

```sh
iconic --sf-symbol-name wind
```

### Full example

Specifying the foreground and background colors, and the target directory:

```sh
iconic --sf-symbol-name wind --foreground-color blue --background-color white --output-directory ~/Dev/MyApp
```

The options are:

```
OPTIONS:
-s, --sf-symbol-name <sf-symbol-name>
						The name of the SFSymbol to use 
-f, --foreground-color <foreground-color>
						Foreground color for the SFSymbol
						Use --list-colors to get a list of colors 
-b, --background-color <background-color>
						Background color for the icon
						Use --list-colors to get a list of colors 
-o, --output-directory <output-directory>
						Output directory; if not given, the current working directory will be used 
-l, --list-colors       List available colors and quit 
-h, --help              Show help information.
```

# Bugs

* the fixed set of colors are terrible
* the randomized color schemes are often terrible
* no support - yet - for watchOS or tvOS

# Credits

The glyph-parsing code was lifted from [Dave DeLong](https://github.com/davedelong/)'s [`sfsymbols`](https://github.com/davedelong/sfsymbols/) repo. Thank-you Dave! 
