//
//  IconGenerator.swift
//  
//  Created by Matthew Flint on 13-May-2020.
//

import Cocoa

struct IconGenerator {
	private let config: IconicArguments

	init(withConfig config: IconicArguments) {
		self.config = config
	}

	func generate(size: CGFloat, scale: CGFloat, glyph: Glyph, colorScheme: ColorScheme, outputURL: URL) throws {
		let imageSize = CGSize(width: size * scale, height: size * scale)
		let translation = CGPoint(x: (imageSize.width - glyph.boundingBox.width) / 2, y: (imageSize.height - glyph.boundingBox.height) / 2)

        let image = NSImage(size: imageSize, flipped: false, drawingHandler: { rect in
            guard let context = NSGraphicsContext.current?.cgContext else { return false }

			context.setFillColor(colorScheme.background)
			context.fill(CGRect(origin: CGPoint(x: 0, y: 0), size: imageSize))

            context.setShouldAntialias(true)
			context.translateBy(x: translation.x, y: translation.y)
			context.addPath(glyph.cgPath)

            context.setFillColor(colorScheme.foreground)
            context.fillPath()
            return true
        })

		try image.pngData.write(to: outputURL)
	}
}

// Taken from Dave DeLong's "sfsymbols" repo
// See https://github.com/davedelong/sfsymbols/blob/master/Sources/SFSymbolsCore/AppKitExtensions.swift
extension NSImage {
    var pngData: Data {
        guard let tiffData = tiffRepresentation else { return Data() }
        guard let bitmap = NSBitmapImageRep(data: tiffData) else { return Data() }
        guard let pngData = bitmap.representation(using: .png, properties: [:]) else { return Data() }
        return pngData
    }
}
